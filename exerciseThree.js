(() => {
    const post = async (userForm) => {
        try {
            await fetch('https://jsonplaceholder.typicode.com/posts', {
                method: "POST",
                body: new FormData(userForm),
            });
            userForm.reset()
        } catch (err) {
            console.log("упс");
        }
    }

    document.getElementById("submit").onclick = (e) => {
        const first = document.getElementsByName("firstName")[0].value
        const last = document.getElementsByName("lastName")[0].value
        e.preventDefault();
        post(document.getElementById("form"))
        alert(`${first} ${last}`)
    }
})();
