(() => {
    const get = async () => {
        try {
            const response = await fetch('https://randomuser.me/api/');
            const data = await response.json();
            return data.results[0]
        } catch (err) {
            console.log("упс");
        }
    }

    document.getElementById("getUser").onclick = async (e) => {
        e.preventDefault();
        const user = await get()
        document.getElementsByName("firstName")[0].value = user.name.first
        document.getElementsByName("lastName")[0].value = user.name.last

    }
})();
